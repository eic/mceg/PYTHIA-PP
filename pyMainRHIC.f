C...As an example, consider a main program of the form
C...Double precision and integer declarations.
*======================================================================
      program pyMainRHIC

      include 'pythia.inc'              ! All PYTHIA commons blocks
      include "mc_set.inc"
      include "py6strf.inc"

      integer NEV, NPRT, ievent, genevent, I, tracknr, pytune
      integer lastgenevent
      REAL p1beam, p2beam, p1mass, p2mass
      REAL XMin, XMax, YMin, YMax, Q2Min, Q2Max
      DOUBLE PRECISION sqrts
      CHARACTER PARAM*100
      CHARACTER OutPutFile*100

c ---------------------------------------------------------------------
c     Run parameter
c ---------------------------------------------------------------------
      integer*4 today(3), now(3)
c---------------------------------------------------------------------
c     ASCII output file
c ---------------------------------------------------------------------
      integer asciiLun
      parameter (asciiLun=29)
      CHARACTER*256 outputfilename
      CHARACTER*256 outname

c---------------------------------------------------------------------
! ... force block data modules to be read
C       external pydata
c ---------------------------------------------------------------------

       p1beam=100. 
       p2beam=100.
       p1mass=PYMASS(2212)
       p2mass=PYMASS(2212)
       ievent=0
       genevent=0
       lastgenevent=0

C...Read switch for output file yes or no
       READ(*,*) outname
C...Read parameters for PYINIT call ( beam and target particle energy).
       READ(*,*) p1beam, p2beam
C...Read number of events to generate, and to print.
       READ(*,*) NEV,NPRT
C...Read min/max x of radgen lookup table
       READ(*,*) XMin,XMax
C...Read min/max y of generation range      
       READ(*,*) YMin,YMax
C...Read min/max Q2 of generation range      
       READ(*,*) Q2Min,Q2Max
C...Read PYTHIA tune parameter
C       READ(*,*) pytune
C       MSTP(5)=pytune
C      CALL PYGIVE(PARAM)
C...Read information for cross section used in radgen
  100  READ(*,'(A)',END=200) PARAM
       CALL PYGIVE(PARAM)
       GOTO 100
c ---------------------------------------------------------------------
C...Initialize PYTHIA.      
c ---------------------------------------------------------------------
  200  write(*,*) '*********************************************'
       write(*,*) 'NOW all parameters are read by PYTHIA'
       write(*,*) '*********************************************'
C       call PYLIST(11)
C       call PYLIST(12)

!     
!     Getting the date and time of the event generation
!
        
      call idate(today)   ! today(1)=day, (2)=month, (3)=year
      call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
        
!      
!     Take date as the SEED for the random number generation
!      
      initseed = today(1) + 10*today(2) + today(3) + now(1) + 5*now(3)
      write(6,*) 'SEED = ', initseed
      call rndmq (idum1,idum2,initseed,' ')

      sqrts=sqrt(4*p1beam*p2beam)
      write(*,*) '*********************************************'
      write(*,*) '1st proton beam energy:', p1beam, 'GeV'
      write(*,*) '2nd proton beam energy:', p2beam, 'GeV'
      write(*,*) 'resulting sqrt(s):', sqrts, 'GeV'
      write(*,*) '*********************************************'
C     lepton is defined in negative z and as beam
      P(1,1)=0.0  
      P(1,2)=0.0  
      P(1,3)=-p1beam
C     proton is defined in positive z and as target
      P(2,1)=0.0
      P(2,2)=0.0
      P(2,3)=p2beam
      call pyinit ('3MOM','p+','p+',WIN)

c ---------------------------------------------------------------------
c     Open ascii output file
c ---------------------------------------------------------------------
       outputfilename=outname
       open(asciiLun, file=outputfilename)
       write(*,*) 'the outputfile will be named: ', outname

c ---------------------------------------------------------------------
C...Event generation loop
c ---------------------------------------------------------------------

C   This is what we write in the ascii-file

        write(29,*)' PYTHIA EVENT FILE '
        write(29,*)'============================================'
        write(29,30) 
30      format('I, ievent, genevent, subprocess, nucleon,
     &  targetparton, xtargparton, beamparton, xbeamparton,
     &  thetabeamprtn, truey, trueQ2, truex, trueW2, trueNu, leptonphi, 
     &  s_hat, t_hat, u_hat, pt2_hat, Q2_hat,  F2, F1, R, sigma_rad, 
     &  SigRadCor, EBrems, photonflux, t-diff, nrTracks')
        write(29,*)'============================================'

        write(29,*)' I  K(I,1)  K(I,2)  K(I,3)  K(I,4)  K(I,5)
     &  P(I,1)  P(I,2)  P(I,3)  P(I,4)  P(I,5)  V(I,1)  V9I,2)  V(I,3)'
        write(29,*)'============================================'

       DO 300 IEV=1,NEV
999      CALL PYEVNT
         if (MSTI(61).eq.1) then
            write(*,*) 'go back to PYEVNT call'
            goto 999
         endif
C         CALL PYLIST(2)

         ievent=IEV       
         genevent=NGEN(0,3)-lastgenevent

         tracknr=N

         write(29,32) 0,ievent,genevent, msti(1), msti(12), 
     &        msti(16), pari(34), msti(15), pari(33), pari(53), 
     &        0., 0., 0., 0., 0., 0.,  
     &        pari(14), pari(15), pari(16), pari(18),  pari(22), 
     &        0., 0., 0., 0., 0., 0., 0., VINT(45), tracknr
 32      format((I4,1x,$),(I10,1x,$),3(I4,1x,$),(I10,1x,$),f9.6,1x,$,
     &         I12,1x,$,
     &         2(f12.6,1x,$),6(f18.11,3x,$),13(f19.9,3x,$),I12,/)
         write(29,*)'============================================'

         DO I=1,tracknr
         if (K(I,3).le.tracknr) then
         write(29,34) I,K(I,1),K(I,2),K(I,3),K(I,4),K(I,5),
     &        P(I,1),P(I,2),P(I,3),P(I,4),P(I,5),
     &        V(I,1),V(I,2),V(I,3)
         endif
         ENDDO
 34      format(2(I6,1x,$),I10,1x,$,3(I6,1x,$),8(f15.6,1x,$),/)
         write(29,*)'=============== Event finished ==============='
         lastgenevent=NGEN(0,3)

  300  CONTINUE
      
C...Print cross sections.
       CALL PYSTAT(1)
       CALL PYSTAT(4)
       CALL PYSTAT(5)
C...Print the Pythia cross section which is needed to get an absolut 
C   normalisation the number is in microbarns
       write(*,*)'==================================================='
       write(*,*)'Pythia total cross section normalisation:',
     +            pari(1)*1000, ' microbarn'
       write(*,*)'Total Number of generated events', MSTI(5)
       write(*,*)'Total Number of trials', NGEN(0,3)
       write(*,*)'NGEN(0,1)', NGEN(0,1)
       write(*,*)'NGEN(0,2)', NGEN(0,2)
       write(*,*)'Now comes the end'
       write(*,*)'==================================================='
       close(29)

C...Check pdf status       
       call PDFSTA
       END
