# PYTHIA-PP

Based on PYTHIA6 (6.4.28) with wrappers and some changes.

Upgraded to cmake.


```shell
cd ${EICDIRECTORY}/PACKAGES
git clone https://gitlab.com/eic/mceg/PYTHIA-PP.git
cd PYTHIA-PP
mkdir build; cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=${EICDIRECTORY}
make -j 8 install
```

## Old instructions, by hand

Requires LHAPDF. Installation instructions for RCF, please adapt to your environment and LHAPDF location.

cd ${EICDIRECTORY}/PACKAGES
git clone https://git.racf.bnl.gov/gitea/EIC/PYTHIA-PP
cd PYTHIA-PP/PYTHIA-6.4.28-mod/
gfortran -c -m64 -fPIC -I../include *.f
ar rcf libpythia6-ppmod.a *.o

# install this library
cp libpythia6-ppmod.a $EICDIRECTORY/lib/

# make and install executable
cd ..
gfortran -c -m64 -fPIC -I./include  *.f
# ignore warning, it applies to a non-standard extension in gfortran

gfortran -m64 -fPIC -I./include  gmc_random.o pyMainRHIC.o \
 -L${EICDIRECTORY}/lib -lpythia6-ppmod -L/cern64/pro/lib -lmathlib -lkernlib -lpacklib_noshift -ldl -lm \
 -L${LHAPDF5} -lLHAPDF -o pythiaRHIC
